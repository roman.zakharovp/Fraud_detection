package ru.psb.hackaton.fd.source;

import org.springframework.stereotype.Component;
import ru.psb.hackaton.fd.data.transaction.Transaction;
import ru.psb.hackaton.fd.data.transaction.TransactionTotals;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class SourceImporterService {

    public TransactionTotals importTotals(String fileName) {
        return new TransactionTotals();
    }

    public Collection<Transaction> importDetails(String fileName) {
        return new ArrayList<>();
    }

}

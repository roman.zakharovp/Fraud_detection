package ru.psb.hackaton.fd.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.psb.hackaton.fd.data.Response;

@RestController("/web/totals/")
public class TotalsController {

    @RequestMapping(value = "/total", method = RequestMethod.GET)
    public Response getTotalInfo() {
        return new Response();
    }

    public Response getBadTransactions() {
        return new Response();
    }

}
